//
//  ViewController.swift
//  mundoanimal_22001697
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Alamofire
import Kingfisher

struct animais: Decodable{
    let name: String
    let image_link:String
    let latin_name: String
}



class ViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nome_latim: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNovoAnimal()
    }
    
    func getNovoAnimal(){
        AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of:animais.self){
            response in
            if let animal = response.value{
                self.image.kf.setImage(with:URL(string:animal.image_link))
                self.title = animal.name
                self.nome_latim.text = animal.latin_name
                
            }
            
        }
    }


}

